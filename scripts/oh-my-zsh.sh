#!/usr/bin/env bash

if [ ! -d ~/.oh-my-zsh ]; then
    mv ~/.zshrc ~/.zshrc.copy
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    rm ~/.zshrc
    mv ~/.zshrc.copy ~/.zshrc
fi

#!/usr/bin/env bash

if [ ! -f ~/.bin/lein ]; then
    curl -o ~/.bin/lein https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
    chmod +x ~/.bin/lein
    ~/.bin/lein
fi

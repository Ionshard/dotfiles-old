# Setup PATH
PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
PATH="${HOME}/.bin:$PATH"
export PATH="$PATH"

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

